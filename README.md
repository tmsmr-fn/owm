# owm
*OpenFaas function that fetches weather forecasts using https://openweathermap.org/*

```
curl -H "Fn-Api-Key: insecure" https://faas.example.com/function/owm -d '{"unit": "C", "lang": "DE", "lat": 48.775845, "lon": 9.182932}'
```
