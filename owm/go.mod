module handler/function

go 1.19

require (
	github.com/briandowns/openweathermap v0.19.0
	golang.org/x/exp v0.0.0-20230304125523-9ff063c70017
)
