package function

import (
	"encoding/json"
	"fmt"
	owm "github.com/briandowns/openweathermap"
	"golang.org/x/exp/slices"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

const (
	// https://openweathermap.org/forecast5
	forecastType = "5"
	// 6 * 3h -> next 18h
	forecastCnt = 6
	iconUrlTpl  = "https://openweathermap.org/img/wn/%s@2x.png"
)

var (
	fnKeys []string
	owmKey string
)

func init() {
	val, present := os.LookupEnv("fn_api_keys")
	if present {
		fnKeys = strings.Split(val, ",")
	}
	val, present = os.LookupEnv("owm_api_key")
	if !present {
		log.Fatalln("missing owm_api_key")
	}
	owmKey = val
}

type ReqBody struct {
	Unit string  `json:"unit"`
	Lang string  `json:"lang"`
	Lat  float64 `json:"lat"`
	Lon  float64 `json:"lon"`
}

type ResDetail struct {
	Dt   time.Time `json:"dt"`
	Temp float64   `json:"temp"`
	Hum  int       `json:"hum"`
	Qpf  float64   `json:"qpf"`
	Wind float64   `json:"wind"`
	Icon string    `json:"icon"`
}

type ResBody struct {
	From    time.Time   `json:"from"`
	To      time.Time   `json:"to"`
	TempMin float64     `json:"tempMin"`
	TempMax float64     `json:"tempMax"`
	Detail  []ResDetail `json:"detail"`
}

func buildResponseData(w *owm.Forecast5WeatherData) ResBody {
	res := ResBody{}
	res.Detail = make([]ResDetail, 0)
	res.From = w.List[0].DtTxt.Time
	res.To = w.List[len(w.List)-1].DtTxt.Time
	res.TempMin = w.List[0].Main.TempMin
	res.TempMax = w.List[0].Main.TempMax
	for _, dt := range w.List {
		if dt.Main.TempMin < res.TempMin {
			res.TempMin = dt.Main.TempMin
		}
		if dt.Main.TempMax > res.TempMax {
			res.TempMax = dt.Main.TempMax
		}
		detail := ResDetail{
			Dt:   dt.DtTxt.Time,
			Temp: dt.Main.Temp,
			Hum:  dt.Main.Humidity,
			Qpf:  dt.Snow.ThreeH + dt.Rain.ThreeH,
			Wind: dt.Wind.Speed,
			Icon: fmt.Sprintf(iconUrlTpl, dt.Weather[0].Icon),
		}
		res.Detail = append(res.Detail, detail)
	}
	return res
}

func Handle(w http.ResponseWriter, r *http.Request) {
	if fnKeys != nil {
		valid := slices.Contains(fnKeys, r.Header.Get("Fn-Api-Key"))
		if !valid {
			http.Error(w, "", http.StatusUnauthorized)
			return
		}
	}
	var params ReqBody
	err := json.NewDecoder(r.Body).Decode(&params)
	defer r.Body.Close()
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	fc, err := owm.NewForecast(forecastType, params.Unit, params.Lang, owmKey)
	loc := owm.Coordinates{
		Longitude: params.Lon,
		Latitude:  params.Lat,
	}
	err = fc.DailyByCoordinates(&loc, forecastCnt)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	weather := fc.ForecastWeatherJson.(*owm.Forecast5WeatherData)
	resData := buildResponseData(weather)
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(resData)
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
