package function

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestCallHandle(t *testing.T) {
	body, _ := json.Marshal(ReqBody{
		Unit: "C",
		Lang: "DE",
		Lat:  48.775845,
		Lon:  9.182932,
	})
	reader := bytes.NewReader(body)
	req, err := http.NewRequest("POST", "/", reader)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Add("Fn-Api-Key", "insecure")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Handle)
	handler.ServeHTTP(rr, req)
	if rr.Code != http.StatusOK {
		t.Fatal(err)
	}
	var weather ResBody
	err = json.NewDecoder(rr.Body).Decode(&weather)
	if rr.Code != http.StatusOK {
		t.Fatal(err)
	}
}
